import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
import { RouterModule } from '@angular/router';
import { CarsService } from './cars/cars.service';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { CarsComponent } from './cars/cars.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { EditCarComponent } from './cars/edit-car/edit-car.component';



@NgModule({
  declarations: [
    AppComponent,
    CarsComponent,
    LoginComponent,
    NotFoundComponent,
    NavigationComponent,
    EditCarComponent,
  
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
     AngularFireModule.initializeApp(environment.firebase),
      FormsModule,
    ReactiveFormsModule,
     RouterModule.forRoot([
      {path:'', component:CarsComponent},
      {path:'cars', component:CarsComponent}, //default - localhost:4200 - homepage
      {path:'login', component:LoginComponent}, //localhost:4200/products
     {path: 'edit-car/:id', component: EditCarComponent},
      {path:'**', component:NotFoundComponent} //all the routs that donwt exist
    ])
   
   

  ],
  providers: [
    CarsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
