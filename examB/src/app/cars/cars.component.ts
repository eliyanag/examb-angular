import { Component, OnInit } from '@angular/core';
import { CarsService } from './cars.service';
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  cars;
  carsKeys = [];

  updateCar(id){
    this.service.getCar(id).subscribe(response=>{
        this.cars = response.json();      
    })
  }
  pessimiaticAdd(){
    this.service.getCars().subscribe(response => {
      this.cars =  response.json();
      this.carsKeys = Object.keys(this.cars);
    });   
  }
   
  deleteCar(key){
    console.log.apply(key);
    let index = this.carsKeys.indexOf(key);
    this.carsKeys.splice(index,1);

    //delete from server
    this.service.deleteCar(key).subscribe(
      response=>console.log(response)
    )
    

    
  }

  constructor(private service:CarsService, private router:Router) {
    this.service.getCars().subscribe(
      response=>{
        this.cars = response.json();
        this.carsKeys = Object.keys(this.cars);
    })
  }

  ngOnInit() {
   /* var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }*/
  }

}