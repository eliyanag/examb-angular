import { Injectable } from '@angular/core';
//Q2
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
//Deployment
import { environment } from './../../environments/environment';


@Injectable()
export class CarsService {
  http:Http; //Q2

   getCars(){
    return this.http.get(environment.url + 'cars');
  }
  //Q5 2
  deleteCar(key){
    return this.http.delete(environment.url + 'cars/'+ key);
  }

  //Q5 1
  putCar(data,key){
    let options = {
      headers: new Headers({
       'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('manufacturer',data.manufacturer).append('model',data.model);
    return this.http.put(environment.url + 'cars/'+ key,params.toString(), options);
  }

  getCar(id){
     return this.http.get(environment.url + 'cars/'+ id);
  }
    //Login Witout JWT
  //Login Witout JWT
/* login(credentials){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
    return this.http.post(environment.url + 'auth',params.toString(),options).map(response=>{
      console.log(environment.url + 'auth');
      let token = response.json().token;
      if(token) localStorage.setItem('token',token);
    })
  }*/


  constructor(http:Http) { 
    this.http = http;
  }

}