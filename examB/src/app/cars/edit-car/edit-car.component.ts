import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { CarsService } from './../cars.service';
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'edit-car',
  templateUrl: './edit-car.component.html',
  styleUrls: ['./edit-car.component.css']
})
export class EditCarComponent implements OnInit {

  @Output() editCar:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() editCarPs:EventEmitter<any> = new EventEmitter<any>();

  manufacturer;
  model;
  service:CarsService;
  car;
  updateform = new FormGroup({
    manufacturer:new FormControl(),
    model:new FormControl()
  });
  constructor(private route: ActivatedRoute ,service: CarsService, private formBuilder: FormBuilder, private router: Router) { 
      this.service = service;
  }

  sendData() {
    //לבדוק name
    this.editCar.emit(this.updateform.value.name);
    console.log(this.updateform.value);

    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putCar(this.updateform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.editCarPs.emit();
          this.router.navigate(['/']);
        }
      );
    })
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getCar(id).subscribe(response=>{
        this.car = response.json();
        console.log(this.car);
        this.manufacturer = this.car.manufacturer;
        this.model = this.car.model ; 
      })
    });
  }
}