import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup } from '@angular/forms';
import { CarsService } from "../cars/cars.service";
import { Router } from "@angular/router";


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  invalid = false;

  loginform = new FormGroup({
	  user:new FormControl(),
	  password:new FormControl(),	    
  });
/*
  login(){
    this.service.login(this.loginform.value).subscribe(response =>{
      this.router.navigate(['/']); 
    }, error =>{
      this.invalid = true; 
    });
  }

  logout(){
    console.log("logout clicked");
    this.invalid = false; 
    localStorage.removeItem('token');
  }*/
  constructor(private router:Router,private service:CarsService) { 

  }

  ngOnInit() {
  }

}
