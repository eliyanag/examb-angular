// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyCwafsmmwELuRGQs_JuAkBUlNbyWuLkww4",
    authDomain: "examb-e9ec8.firebaseapp.com",
    databaseURL: "https://examb-e9ec8.firebaseio.com",
    projectId: "examb-e9ec8",
    storageBucket: "examb-e9ec8.appspot.com",
    messagingSenderId: "597861715336"
  }
};